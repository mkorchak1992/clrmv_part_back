import {HttpException, HttpStatus, Injectable} from "@nestjs/common";
import {UserService} from '../user/user.service';
import RegisterDto from './dto/register.dto';
import * as bcrypt from 'bcrypt';
import {ConfigService} from '@nestjs/config';
import {JwtService} from '@nestjs/jwt';
import {TokenPayload} from './tokenPayload.interface';

@Injectable()
export class AuthService {
    constructor(private readonly userService: UserService,
                private readonly jwtService: JwtService,
                private readonly  configService: ConfigService
    ) {
    }

    public getCookies(userId: string) {
        const payload: TokenPayload = {userId};
        const token = this.jwtService.sign(payload);
        return `Authentication =${token}; `
    }

    public logOut() {
        return `Authentication=; HttpOnly; Path=/;`;
    }

    public async register(registrationData: RegisterDto) {
        const hashedPassword = await bcrypt.hash(registrationData.password, 10)
        try {
            const createdUser = await this.userService.create({
                ...registrationData,
                password: hashedPassword
            });
            createdUser.password = undefined;
            return createdUser;
        } catch (err) {
            throw new HttpException('bad data', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private async verifyPassword(plainTextPassword: string, hashedPassword: string) {
        const isPasswordMatch = await bcrypt.compare(
            plainTextPassword,
            hashedPassword
        );
        if (!isPasswordMatch) {
            throw new HttpException('wrong data', HttpStatus.BAD_REQUEST);
        }
    }

    public async getAuthUser(email: string, plainTextPassword: string) {
        try {
            const user = await this.userService.getByEmail(email);
            await this.verifyPassword(plainTextPassword, user.password);
            user.password = undefined;
            return user;
        } catch (error) {
            throw new HttpException('wrong data', HttpStatus.BAD_REQUEST);
        }
    }

}