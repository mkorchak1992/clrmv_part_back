import {Module} from '@nestjs/common';
import {UserModule} from "../user/user.module";
import {PassportModule} from "@nestjs/passport";
import {AuthService} from './auth.service';
import {LocalStrategy} from './local.strategy';
import {AuthController} from './auth.controller';
import {JwtModule} from '@nestjs/jwt';
import {ConfigModule, ConfigService} from '@nestjs/config';
import {JwtStrategy} from './jwt.strategy';

@Module({
    imports: [
        JwtModule.registerAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
                secret: configService.get('JWT_SECRET'),
                signOptions: {
                    expiresIn: `${configService.get('JWT_EXPIRE')}`,
                },
            }),
            inject: [ConfigService]
        }),
        UserModule,
        PassportModule,
        ConfigModule,
    ],
    providers: [AuthService, LocalStrategy, JwtModule],
    controllers: [AuthController],
    exports: [JwtModule]
})
export class AuthModule {}