import {Body, Controller, Get, HttpCode, Post, Req, UseGuards} from "@nestjs/common";
import {AuthService} from './auth.service';
import RegisterDto from './dto/register.dto';
import ReqUser from '../requests/reqUser.interface';
import {localAuthGuard} from './auth.guard';
import JwtAuthGuard from './jwtAuth.guard';


@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {
    }

    @Post('register')
    register(@Body() regData: RegisterDto) {
        return this.authService.register(regData)
    }

    @HttpCode(200)
    @UseGuards(localAuthGuard)

    @Post('login')
    login(@Req() request: ReqUser) {
        const {user} = request;
        const cookie = this.authService.getCookies(user.id);
        request.res.setHeader('Set-Cookie', cookie);
        user.password = undefined;
        return user;
    }

    @UseGuards(JwtAuthGuard)

    @Post('logout')
    logOut(@Req() request: ReqUser) {
        request.res.setHeader('Set-Cookie', this.authService.logOut());
    }

    @UseGuards(JwtAuthGuard)
    @Get('')
    authenticate(@Req() request: ReqUser) {
        const user = request.user;
        user.password = undefined;
        return user;
    }
}