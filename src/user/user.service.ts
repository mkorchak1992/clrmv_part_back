import {HttpException, HttpStatus,Injectable} from "@nestjs/common";
import {InjectRepository} from '@nestjs/typeorm';
import {User} from './user.entity';
import {Repository} from 'typeorm';
import {CreateUserDto} from './dto/createuser.dto';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User) private userRepository: Repository<User>,) {
    }

    async getUserInfo(id: string) {
        const user = await this.userRepository.findOne(id);
        user.password = undefined
        if (user) return {
            ...user,
        };
    }

    async getById(id: string) {
        const user = await this.userRepository.findOne({id});
        if (user) {
            return user;
        }
        throw new HttpException('User does not exist', HttpStatus.NOT_FOUND);
    }

    async getByEmail(email: string) {
        const user = await this.userRepository.findOne({email});
        if (user) {
            return user;
        }
        throw new HttpException('User does not exist', HttpStatus.NOT_FOUND);
    }

    async create(userData: CreateUserDto) {
        const newUser = await this.userRepository.create(userData);
        await this.userRepository.save(newUser);
        return newUser;
    }
}
