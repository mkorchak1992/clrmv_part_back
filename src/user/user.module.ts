import {Module} from '@nestjs/common';
import {UserService} from "./user.service";
import {UserResolver} from './user.resolver';
import {TypeOrmModule} from '@nestjs/typeorm';
import {User} from './user.entity';
import {UserController} from './user.controller';

@Module({
    imports: [TypeOrmModule.forFeature([User])],
    exports: [UserService, TypeOrmModule],
    providers: [UserService],
    controllers: [UserResolver],
})
export class UserModule {
}