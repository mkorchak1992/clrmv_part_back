import {Module} from '@nestjs/common';
import {CommentService} from './comment.service';
import {CommentResolver} from './comment.resolver';
import {TypeOrmModule} from '@nestjs/typeorm';
import {Comment} from './comment.entity';
import {CommentController} from './comment.controller';


@Module({
    imports: [TypeOrmModule.forFeature([Comment])],
    providers: [CommentService, CommentResolver],
    controllers: [CommentController],
    exports: [CommentService, TypeOrmModule],
})

export class CommentModule {
}