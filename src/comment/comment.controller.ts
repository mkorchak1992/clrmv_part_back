import {Body, Controller, Delete, HttpCode, Post, Req, UseGuards} from '@nestjs/common';
import JwtAuthGuard from '../auth/jwtAuth.guard';
import ReqUser from '../requests/reqUser.interface';
import {CommentService} from './comment.service';
import {CreateCommentDto} from './dto/createComment.dto';
import ReqPost from '../requests/reqPost.interface';
import {ACGuard} from 'nest-access-control';


@Controller('comment')
export class CommentController {
    constructor(private readonly commentService: CommentService) {
    }

    @HttpCode(200)
    @UseGuards(JwtAuthGuard, ACGuard)
    @Post('create')
    async createComment(@Body() commentData: CreateCommentDto, @Req() reqPost: ReqPost, @Req() reqUser: ReqUser) {
        return this.commentService.create(commentData, reqPost.post, reqUser.user)
    }


    @UseGuards(JwtAuthGuard, ACGuard)
    @Delete('delete')
    async deleteComment(@Body() commentData: CreateCommentDto) {
        return this.commentService.delete(commentData)
    }

}
