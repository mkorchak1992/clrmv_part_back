import {
    Column, CreateDateColumn,
    Entity, JoinTable, ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {Post} from '../post/post.entity';
import {User} from '../user/user.entity';
import {Field, ObjectType} from '@nestjs/graphql';

@Entity('comment')
@ObjectType()
export class Comment {
    @PrimaryGeneratedColumn()
    @Field()
    id: string;

    @ManyToOne((type) => Post, post => post.comments)
    @Field(type => Post)
    @JoinTable()
    author: User;


    @Column()
    @Field()
    text: string;

    @CreateDateColumn({name: 'created'})
    @Field()
    created: Date;

}